"""
Parts of this code is originally from the: TORCHVISION OBJECT DETECTION FINETUNING TUTORIAL (https://pytorch.org/tutorials/intermediate/torchvision_tutorial.html).
"""
from pathlib import Path

import torch
import torchvision
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.mask_rcnn import MaskRCNNPredictor

from test_project_mlops.references.detection.engine import evaluate, train_one_epoch


class MaskRCNNModel:
    def __init__(self, num_classes: int):
        # load an instance segmentation model pre-trained on COCO
        model = torchvision.models.detection.maskrcnn_resnet50_fpn(weights="DEFAULT")

        # get number of input features for the classifier
        in_features = model.roi_heads.box_predictor.cls_score.in_features
        # replace the pre-trained head with a new one
        model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)

        # now get the number of input features for the mask classifier
        in_features_mask = model.roi_heads.mask_predictor.conv5_mask.in_channels
        hidden_layer = 256
        # and replace the mask predictor with a new one
        model.roi_heads.mask_predictor = MaskRCNNPredictor(in_features_mask, hidden_layer, num_classes)
        self.model = model

    def train(self, num_epochs: int, optimizer, lr_scheduler, data_loader, data_loader_test):  # TODO typing
        for epoch in range(num_epochs):
            # train for one epoch, printing every 10 iterations
            train_one_epoch(self.model, optimizer, data_loader, self.device, epoch, print_freq=10)
            # update the learning rate
            lr_scheduler.step()
            # evaluate on the test dataset
            evaluate(self.model, data_loader_test, device=self.device)

    def evaluate(self, data_loader):
        return evaluate(self.model, data_loader, device=self.device)

    def to_device(self, device):
        self.device = device
        self.model.to(device)

    def get_params(self):
        return [p for p in self.model.parameters() if p.requires_grad]

    def forward(self, images: torch.Tensor) -> torch.Tensor:
        return self.model(images)

    def eval(self):
        self.model.eval()

    def save_checkpoint(self, dirpath: str) -> None:
        path = Path(dirpath) / "checkpoint.ckpt"
        torch.save(self.model.state_dict(), path)

    def load_checkpoint(self, checkpoint_path: str) -> None:
        self.model.load_state_dict(torch.load(checkpoint_path))
