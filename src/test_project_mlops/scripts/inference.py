"""
Script allows for inference with the use of fine-tuned pytorch mark-rcnn object detection model.

Usage:
>>> python inference.py --images_root_directory ../../../data/PennFudan/PNGImages --num_classes 2 --checkpoint_path ../../../experiment/checkpoint.ckpt --experiment_directory ../../../experiment/output_images
"""
import os
from argparse import ArgumentParser

import torch
from PIL import Image, ImageDraw
from torchvision.transforms.functional import convert_image_dtype, pil_to_tensor
from tqdm import tqdm

from test_project_mlops.model.mask_rcnn import MaskRCNNModel


def prepare_output(image: Image, results: dict) -> Image:
    [result] = results
    boxes = result["boxes"]
    scores = result["scores"]
    masks = result["masks"]
    output = ImageDraw.Draw(image)
    for box, mask, score in zip(boxes, masks, scores):
        if score > 0.9:
            output.rectangle(box.detach().numpy(), outline="red")
    return output


def save_image(image: Image, image_name: str, path: str):
    output_path = os.path.join(path, image_name)
    image.save(output_path)


def main(images_root_directory: str, checkpoint_path: str, num_classes: int, experiment_directory: str):
    # train on the GPU or on the CPU, if a GPU is not available
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

    # our dataset has two classes only - background and person
    num_classes = num_classes
    # get the model using our helper function
    model = MaskRCNNModel(num_classes)
    # load trained weights
    model.load_checkpoint(checkpoint_path)
    # move model to the right device
    model.to_device(device)
    # set model in eval state
    model.eval()

    os.makedirs(experiment_directory, exist_ok=True)
    images_paths = os.listdir(images_root_directory)
    pbar = tqdm(total=len(images_paths))
    for name in images_paths:
        image_path = os.path.join(images_root_directory, name)
        image = Image.open(image_path).convert("RGB")

        tensor = convert_image_dtype(pil_to_tensor(image), torch.float)
        batch = torch.unsqueeze(tensor, dim=0)
        results = model.forward(batch)
        prepare_output(image, results)
        save_image(image, name, experiment_directory)
        pbar.update(1)
    pbar.close()

    print("That's it!")


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--images_root_directory", metavar="path/to/directory", type=str)
    parser.add_argument("--checkpoint_path", metavar="path/to/file.ckpt", type=str)
    parser.add_argument("--num_classes", type=int)
    parser.add_argument("--experiment_directory", metavar="path/to/directory", type=str)
    args = parser.parse_args()

    main(
        args.images_root_directory,
        args.checkpoint_path,
        args.num_classes,
        args.experiment_directory,
    )
