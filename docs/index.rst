.. Documentation master file
   You can adapt this file completely to your preferences, but it should at least
   contain the root `toctree` directive.

ds - test_project_mlops
====================================================================================================================

Documentation for `test_project_mlops` python package.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   project_overview
   code_documentation
   howtos
   licenses



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
