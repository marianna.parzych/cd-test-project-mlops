# Code documentation

```{hint}

  To add your code use sphinx tool in project root directory:

    $ sphinx-apidoc -o docs/api/ src/test_project_mlops

  and add reference from any page which is reachable from the index page.
```

```python
    import test_project_mlops
```

```{toctree}
---
maxdepth: 4
---
api/modules
```