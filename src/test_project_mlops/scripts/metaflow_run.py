from metaflow import FlowSpec, pip, project, resources, step


@project(name="project-name")
class ObjectDetectionPipeline(FlowSpec):
    @resources(
        cpu=1,
        gpu=1,
        memory=8000,  # TODO on cpu now, can't check memory usage
    )
    @pip(
        requirements_path="../../../requirements.txt",
    )
    @step
    def start(self):
        self.next(self.training)

    @step
    def training(self):
        from training import main

        dataset_root_directory = "../../../data/PennFudan"
        num_classes = 2
        experiment_directory = "../../../experiment"
        num_epochs = 10
        main(dataset_root_directory, num_classes, experiment_directory, num_epochs)

        self.next(self.evaluation)

    @step
    def evaluation(self):
        from evaluate import main

        dataset_root_directory = "../../../data/PennFudan"
        checkpoint_path = "../../../experiment/checkpoint.ckpt"
        num_classes = 2
        experiment_directory = "../../../experiment"
        main(dataset_root_directory, checkpoint_path, num_classes, experiment_directory)

        self.next(self.inference)

    @step
    def inference(self):
        from inference import main

        images_root_directory = "../../../data/PennFudan/PNGImages"
        checkpoint_path = "../../../experiment/checkpoint.ckpt"
        num_classes = 2
        experiment_directory = "../../../experiment/output_images"
        main(images_root_directory, checkpoint_path, num_classes, experiment_directory)

        self.next(self.end)

    @step
    def end(self):
        pass


if __name__ == "__main__":
    ObjectDetectionPipeline()
