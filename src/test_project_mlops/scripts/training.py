"""
Script allows for fine-tuning of pytorch mark-rcnn object detection model.
Parts of this code is originally from the: TORCHVISION OBJECT DETECTION FINETUNING TUTORIAL (https://pytorch.org/tutorials/intermediate/torchvision_tutorial.html).

Usage:
>>> python training.py --dataset_root_directory ../../../data/PennFudan --num_classes 2 --experiment_directory ../../../experiment --num_epochs 10
"""
from argparse import ArgumentParser

import torch

import test_project_mlops.references.detection.transforms as T
import test_project_mlops.references.detection.utils as utils
from test_project_mlops.dataset.pennfudan_dataset import PennFudanDataset
from test_project_mlops.model.mask_rcnn import MaskRCNNModel


def get_transform(train: bool):
    transforms = []
    transforms.append(T.PILToTensor())
    transforms.append(T.ConvertImageDtype(torch.float))
    if train:
        transforms.append(T.RandomHorizontalFlip(0.5))
    return T.Compose(transforms)


def main(dataset_root_directory: str, num_classes: int, experiment_directory: str, num_epochs: int = 10):
    # train on the GPU or on the CPU, if a GPU is not available
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

    # our dataset has two classes only - background and person
    num_classes = num_classes
    # use our dataset and defined transformations
    dataset = PennFudanDataset(dataset_root_directory, get_transform(train=True))
    dataset_test = PennFudanDataset(dataset_root_directory, get_transform(train=False))

    # split the dataset in train and test set
    torch.manual_seed(42)
    torch.cuda.manual_seed(42)

    indices = torch.randperm(len(dataset)).tolist()
    dataset = torch.utils.data.Subset(dataset, indices[:-50])
    dataset_test = torch.utils.data.Subset(dataset_test, indices[-50:])

    # define training and validation data loaders
    data_loader = torch.utils.data.DataLoader(
        dataset, batch_size=2, shuffle=True, num_workers=4, collate_fn=utils.collate_fn
    )

    data_loader_test = torch.utils.data.DataLoader(
        dataset_test, batch_size=1, shuffle=False, num_workers=4, collate_fn=utils.collate_fn
    )

    # get the model using our helper function
    model = MaskRCNNModel(num_classes)

    # move model to the right device
    model.to_device(device)

    # construct an optimizer
    params = model.get_params()
    optimizer = torch.optim.SGD(params, lr=0.005, momentum=0.9, weight_decay=0.0005)
    # and a learning rate scheduler
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=3, gamma=0.1)
    # let's train it
    model.train(num_epochs, optimizer, lr_scheduler, data_loader, data_loader_test)

    os.makedirs(experiment_directory, exist_ok=True)
    model.save_checkpoint(experiment_directory)

    print("That's it!")


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--dataset_root_directory", metavar="path/to/directory", type=str)
    parser.add_argument("--num_classes", type=int)
    parser.add_argument("--experiment_directory", metavar="path/to/directory", type=str)
    parser.add_argument("--num_epochs", type=int)
    args = parser.parse_args()

    main(args.dataset_root_directory, args.num_classes, args.experiment_directory, args.num_epochs)
