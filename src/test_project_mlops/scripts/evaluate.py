"""
Script allows for evaluation of the fine-tuned pytorch mark-rcnn object detection model.
Parts of this code is originally from the: TORCHVISION OBJECT DETECTION FINETUNING TUTORIAL (https://pytorch.org/tutorials/intermediate/torchvision_tutorial.html).

Usage:
>>> python evaluate.py --dataset_root_directory ../../../data/PennFudan --num_classes 2 --checkpoint_path ../../../experiment/checkpoint.ckpt --experiment_directory ../../../experiment
"""
import io
from argparse import ArgumentParser
from contextlib import redirect_stdout
from pathlib import Path

import torch

import test_project_mlops.references.detection.utils as utils
from test_project_mlops.dataset.pennfudan_dataset import PennFudanDataset
from test_project_mlops.model.mask_rcnn import MaskRCNNModel
from test_project_mlops.scripts.training import get_transform


def prepare_evaluation_output(checkpoint_path, dataset_root_directory, images_list, coco_evaluator) -> str:
    eval_str = []
    eval_str.append(f"Evaluated model: {checkpoint_path}")
    eval_str.append(f"Evaluation on dataset: {dataset_root_directory}")
    eval_str.append(f"Images in validation split: ")
    for image in images_list:
        eval_str.append(f"    {image}")
    eval_str.append(f"----------------------------")
    eval_str.append(f"Metrics:")
    f = io.StringIO()
    for iou_type, coco_eval in coco_evaluator.coco_eval.items():
        eval_str.append(f"IoU metric: {iou_type}")
        with redirect_stdout(f):
            coco_eval.summarize()
        eval_str.extend(f.getvalue().split("\n"))
    return eval_str


def save_output(output_str, dirpath):
    file_path = Path(dirpath) / f"evaluation.txt"
    with open(file_path, "w") as f:
        for line in output_str:
            f.write(f"{line}\n")


def main(dataset_root_directory: str, checkpoint_path: str, num_classes: int, experiment_directory: str):
    # train on the GPU or on the CPU, if a GPU is not available
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

    # our dataset has two classes only - background and person
    num_classes = num_classes
    # get the model using our helper function
    model = MaskRCNNModel(num_classes)
    # load trained weights
    model.load_checkpoint(checkpoint_path)
    # move model to the right device
    model.to_device(device)

    # use our dataset and defined transformations
    dataset_test = PennFudanDataset(dataset_root_directory, get_transform(train=False))

    # get only the test part of the set
    torch.manual_seed(42)
    torch.cuda.manual_seed(42)

    indices = torch.randperm(len(dataset_test)).tolist()[-50:]
    images_list = dataset_test.list_images(indices)
    dataset_test = torch.utils.data.Subset(dataset_test, indices)

    # define data loader
    data_loader_test = torch.utils.data.DataLoader(
        dataset_test, batch_size=1, shuffle=False, num_workers=4, collate_fn=utils.collate_fn
    )

    coco_evaluator = model.evaluate(data_loader_test)
    output_str = prepare_evaluation_output(checkpoint_path, dataset_root_directory, images_list, coco_evaluator)

    os.makedirs(experiment_directory, exist_ok=True)
    save_output(output_str, experiment_directory)

    print("That's it!")


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--dataset_root_directory", metavar="path/to/directory", type=str)
    parser.add_argument("--checkpoint_path", metavar="path/to/file.ckpt", type=str)
    parser.add_argument("--num_classes", type=int)
    parser.add_argument("--experiment_directory", metavar="path/to/directory", type=str)
    args = parser.parse_args()

    main(
        args.dataset_root_directory,
        args.checkpoint_path,
        args.num_classes,
        args.experiment_directory,
    )
