# Test project for ml-ops platform integration

This repo is based on [TORCHVISION OBJECT DETECTION FINETUNING TUTORIAL](https://pytorch.org/tutorials/intermediate/torchvision_tutorial.html).

## Preparation of the data

Download the [Penn-Fudan Database for Pedestrian Detection and Segmentation](https://www.cis.upenn.edu/~jshi/ped_html/). Extract the zip file to the `data/` folder, so we have the following folder structure:

```
├── data
│   ├── PennFudanPed
│   │   ├── PedMasks
│   │   │   ├── FudanPed00001_mask.png
│   │   │   ├── FudanPed00002_mask.png
│   │   │   ├── FudanPed00003_mask.png
│   │   │   ├── FudanPed00004_mask.png
│   │   │   ├── ...
│   │   ├── PNGImages
│   │   │   ├── FudanPed00001.png
│   │   │   ├── FudanPed00002.png
│   │   │   ├── FudanPed00003.png
│   │   │   ├── FudanPed00004.png
│   │   │   ├── ...
```

## Installation

To use this repo, the following installation steps are required:  # TODO

## Scripts

Training:
```bash
cd src/test_project_mlops/scripts
python training.py --dataset_root_directory ../../../data/PennFudan --num_classes 2 --experiment_directory ../../../experiment --num_epochs 10
```

Evaluation:
```bash
cd src/test_project_mlops/scripts
python evaluate.py --dataset_root_directory ../../../data/PennFudan --checkpoint_path ../../../experiment/checkpoint.ckpt --num_classes 2 --experiment_directory ../../../experiment
```

Inference:
```bash
cd src/test_project_mlops/scripts
python inference.py --images_root_directory ../../../data/PennFudan/PGNImages --checkpoint_path ../../../experiment/checkpoint.ckpt --num_classes 2 --experiment_directory ../../../experiment/images_output
```

## Metaflow

Local execution:

```bash
cd src/test_project_mlops/scripts
python metaflow_run.py run
```

Remote execution:

```bash
cd src/test_project_mlops/scripts
python metaflow_run.py run --with kubernetes
```